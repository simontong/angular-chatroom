var extend = require('extend'),
    Db = require(__dirname + '/../lib/Db'),
    Model = require(__dirname + '/../lib/Model')();

module.exports = extend(Model, {
    tableName: function () {
        return '`user`';
    },
    primaryKey: function () {
        return '`user_id`';
    },

    /**
     * Login
     * @param username
     * @param password
     * @param next
     */
    authenticate: function (username, password, next) {
        var sql = "SELECT * FROM `user` WHERE username=? AND password=?";
        Db.query(sql, [username, password], function (err, res) {
            next(err, res[0]);
        });
    },

    getUser: function (userId, next) {
        var sql = "SELECT user_id,username,status_id,colour FROM " + this.tableName() + " WHERE " + this.primaryKey() + "=?";
        Db.query(sql, [userId], function (err, res) {
            next(err, res[0]);
        });
    }
});
