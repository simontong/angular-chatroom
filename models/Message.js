var extend = require('extend'),
    Db = require(__dirname + '/../lib/Db'),
    Model = require(__dirname + '/../lib/Model')();

module.exports = extend(Model, {
    tableName: function () {
        return '`message`';
    },
    primaryKey: function () {
        return '`message_id`';
    },

    /**
     * Get messages for sending down the line
     * @param next
     */
    getMessages: function (next) {
        var sql = "SELECT m.message_id,m.status_id,m.message,m.highlight,m.date_added,u.username,u.colour AS `user_colour`\
            FROM message m, user u WHERE m.user_id = u.user_id\
            ORDER BY m.date_added DESC\
            LIMIT 100";
        Db.query(sql, function (err, res) {
            if (err) throw err;
            next(res);
        });
    },

    /**
     *
     * @param id
     * @param next
     */
    getMessage: function (id, next) {
        var sql = "SELECT m.message_id,m.status_id,m.message,m.highlight,m.date_added,u.username,u.colour AS `user_colour`\
            FROM message m, user u WHERE m.user_id = u.user_id AND m.message_id=?";
        Db.query(sql, [id], function (err, res) {
            if (err) throw err;
            next(res[0]);
        });
    },

    /**
     *
     * @param data
     * @param next
     */
    save: function (data, next) {
        var that = this,
            sql = 'INSERT INTO message SET user_id=?, message=?, date_added=NOW(),date_updated=NOW()';
        Db.query(sql, [data.user_id, data.message], function (err, res) {
            if (err) throw err;
            console.log(res);

            that.getMessage(res.insertId, function (res) {
                next(res);
            });
        });
    },

    /**
     * Format messages
     * @param messages
     */
    format: function (messages) {
        messages.forEach(function (item) {
            item.utDateAdded = (item.date_added) ? item.date_added.getTime() : null;
        });

        return messages;
    }
});
