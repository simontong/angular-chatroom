var extend = require('extend'),
    Db = require(__dirname + '/../lib/Db'),
    Model = require(__dirname + '/../lib/Model')();

module.exports = extend(Model, {
    tableName: function () {
        return '`file`';
    },
    primaryKey: function () {
        return '`file_id`';
    }
});
