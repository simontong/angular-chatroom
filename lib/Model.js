var Db = require(__dirname + '/../lib/Db');

module.exports = function () {
    return {
        tableName: function () { throw 'TableName must be overridden.'; },
        primaryKey: function () { throw 'PrimaryKey must be overridden.'; },

        /**
         * Find user by id
         * @param id Number
         * @param next Function
         */
        findByPk: function (id, next) {
            var sql = "SELECT * FROM " + this.tableName() + " WHERE " + this.primaryKey() + "=?";
            Db.query(sql, [id], function (err, res) {
                next(err, res[0]);
            });
        },

        /**
         * Find all
         * @param next
         */
        findAll: function (next) {
            Db.query("SELECT * FROM " + this.tableName() + " LIMIT 50", function (err, rows) {
                next(err, rows);
            });
        }
    }
};