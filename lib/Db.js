var cfg = require(__dirname + '/../config/main'),
    mysql = require('mysql');

var connection = mysql.createConnection(cfg.db);
connection.connect();

/*connection.config.queryFormat = function (query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, function (txt, key) {
        if (values.hasOwnProperty(key)) {
            return this.escape(values[key]);
        }
        return txt;
    }.bind(this));
};*/

module.exports = connection;