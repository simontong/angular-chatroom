var express = require('express'),
    app = express(),
    cfg = require('./config/main'),
    MySQLStore = require('connect-session-store-mysql')(express),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    less = require('less-middleware'),
    path = require('path'),
    passportSocketIo = require('passport.socketio'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    site = require('./controllers/site'),
    User = require('./models/User'),
    Message = require('./models/Message'),
    clients = { sockets: [], user: [] } // socket stuff
    sessionStore = new MySQLStore(cfg.db, {
//            expire: 3000
        tableName: 'session',
        col: {
            sid: 'id',
            data: 'data',
            expire: 'expire'
        }
    })
    ;

server.listen(3000);

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.authenticate(username, password, function (err, user) {
            if (err) throw err;
            if (user && user.user_id) {
                return done(null, user);
            }
            return done(null, false, { message: 'Incorrect username.'});
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user.user_id);
});

passport.deserializeUser(function (id, done) {
    User.getUser(id, function (err, user) {
        done(err, user);
    });
});

var auth = function (req, res, next) {
    if (!req.session.passport.user) {
//        req.session.redirect_to = '/';
        res.redirect('/login');
    } else {
        next();
    }
}


// set up app
app.configure(function () {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(less({ src: __dirname + '/www', compile: true }));
    app.use(express.static(path.join(__dirname, 'www')));
    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.favicon());
    app.use(express.session({
        secret: '123abc',
        key: 'express.sid',
        store: sessionStore
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(app.router);
    app.use(express.logger('local'));
});

// set up routes
app.get('/', auth, site.index);
app.get('/chat', auth, site.chat);
app.get('/files', auth, site.files);

app.get('/login', site.login);
app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}));


io.set("authorization", passportSocketIo.authorize({
    key: 'express.sid', //the cookie where express (or connect) stores its session id.
    secret: '123abc', //the session secret to parse the cookie
    store: sessionStore,
    fail: function (data, accept) {     // *optional* callbacks on success or fail
        accept(null, false);             // second param takes boolean on whether or not to allow handshake
    },
    success: function (data, accept) {
        accept(null, true);
    }
}));

io.sockets.on('connection', function (client) {

    clients.sockets.push(client);
    clients.user.push(client.handshake.user);

    // broadcast to connected users that this user connected
    client.broadcast.emit('user:connect', [client.handshake.user]);

    // send all users connected to client who connected
    client.emit('user:connect', clients.user);

    client.on('user:typing', function (isTyping) {
        client.broadcast.emit('user:typing', {
            user: client.handshake.user,
            isTyping: isTyping
        });
    });

    client.on('disconnect', function () {
        var i = clients.sockets.indexOf(this);
        client.broadcast.emit('user:disconnect', clients.user[i]);
        clients.sockets.splice(i, 1);
        clients.user.splice(i, 1);
    });

    client.on('message', function (message) {
        Message.save({ user_id: client.handshake.user.user_id, message: message.data }, function (res) {
            io.sockets.emit('message', Message.format([res]));
        });
    });
});