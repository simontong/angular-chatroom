var Message = require('../models/Message.js'),
    User = require('../models/User.js'),
    File = require('../models/File.js');

exports.index = function (req, res) {
    Message.getMessages(function (messages) {
        File.findAll(function (files) {
            res.render('site/index', {
                title: 'Chat',
                messages: Message.format(messages),
                files: files,
                pretty: true
            });
        });
    });
};

exports.login = function (req, res) {
    res.render('site/login', {
        title: 'Login'
    });

};

exports.chat = function (req, res) {
    res.render('site/_chat');
};

exports.files = function (req, res) {
    res.render('site/_files');
};