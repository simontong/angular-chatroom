var chatApp = angular.module('chatApp', ['ui']);

chatApp.config(function ($routeProvider) {
    $routeProvider
        .when('/chat', {
            templateUrl: '/chat',
            controller: ChatCtrl,
            activeTab: 'chat'
        })
        .when('/files', {
            templateUrl: '/files',
            controller: FilesCtrl,
            activeTab: 'files'
        })
        .otherwise({ redirectTo: '/chat' });
});
