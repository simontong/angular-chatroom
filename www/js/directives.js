
/**
 * Resizeable boxes
 */
chatApp.directive('resizer', function () {
    return {
        restrict: 'E',
        link: function (scope, el, attrs) {
            var axis = attrs.axis,
                resize = attrs.resize,
                shrink = attrs.shrink;

            el.draggable({
                containment: 'body',
                axis: axis,
                stop: function () {
                    var size;
                    if (axis == 'y') {
                        size = $(document).height() - el.offset().top + 'px';
                        $(resize).css('height', size);
                        $(shrink).css('bottom', size);
                        el.css({ bottom: 'auto', top: 0 });
                    }
                    else {
                        size = el.offset().left + 'px';
                        $(resize).css('width', size);
                        $(shrink).css('left', size);
                        el.css({ left: 'auto', right: 0 });
                    }
                }
            });
        }
    };
});

/**
 * Display date as nice
 */
chatApp.directive('niceTime', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            function pad(number) {
                return (number < 10 ? '0' : '') + number
            }

            scope.$watch(attr.niceTime, function (milliseconds) {
                var inDate = new Date(milliseconds),
                    nowDate = new Date(),
                    year = inDate.getFullYear(),
                    month = inDate.getMonth()+1,
                    day = inDate.getDate(),
                    hour = inDate.getHours(),
                    mHour = (hour >= 12) ? hour-12 : hour,
                    minute = inDate.getMinutes(),
                    second = inDate.getSeconds(),
                    meridian = hour < 12 ? 'AM' : 'PM',
                    date = [pad(day), pad(month), year].join('/'),
                    time = [mHour, pad(minute), pad(second)].join(':') + ' ' + meridian,
                    compareInDate = new Date(milliseconds);

                compareInDate.setHours(0,0,0,0);
                nowDate.setHours(0,0,0,0);

                // call setHours to take the time out of the comparison
                if (compareInDate.toString() == nowDate.toString()) {
                    el.text(time);
                }
                else {
                    el.text(date + ' ' + time);
                }
            });
        }
    };
});

/**
 * Get user status icon
 */
chatApp.directive('statusIcon', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            scope.$watch(attr.statusIcon, function (value) {
                var statusIcon = '';
                switch (value) {
                    default:
                        statusIcon = 'icon-user';
                        break;

                    case '1': // away
                        statusIcon = 'icon-minus-sign';
                        break;
                    case '2':
                        statusIcon = 'icon-off';
                        break;
                }
                el.attr('class', statusIcon);
            });
        }
    };
});


/**
 * Auto scroll
 */
chatApp.directive('autoscroll', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            var _el = el[0];
            scope.$watch(function () { return _el.scrollHeight; }, function () {
                if (el.attr('autoscroll')) {
                    _el.scrollTop = _el.scrollHeight - _el.clientHeight;
                }
            });

            el.on('scroll', function () {
                el.attr('autoscroll', (_el.scrollTop == _el.scrollHeight - _el.clientHeight) ? '1' : '');
            });
        }
    };
});

/**
 * Unseen count
 */
chatApp.directive('unseenCount', function (Message, File) {
    return {
        restrict: 'E',
        replace: true,
        template: '<span class="badge badge-important">{{count}}</span>',
        scope: {},
        link: function (scope, el, attr) {

            if (attr.type == 'files') {

                scope.$watch(function () { return File.unseen; }, function () {
                    if (File.unseen > 0) {
                        el.show();
                        scope.count = File.unseen;
                    }
                    else {
                        el.hide();
                    }
                });

            }
            else if (attr.type == 'message') {

                scope.$watch(function () { return Message.unseen; }, function () {
                    if (Message.unseen > 0) {
                        el.show();
                        scope.count = Message.unseen;
                    }
                    else {
                        el.hide();
                    }
                });
            }

        }
    }
});