
/**
 * Users service
 */
chatApp.factory('User', function() {
    return {
        userIds: [],
        users: [],

        set: function (users) {
            var that = this;
            angular.forEach(users, function (val) {
                if (val && val.user_id && that.userIds.indexOf(val) === -1) {
                    that.userIds.push(val.user_id);
                    that.users[val.user_id] = val;
                }
            });
        },

        remove: function (userId) {
            var i = this.userIds.indexOf(userId);
            if (i !== -1) {
                this.userIds.splice(i, 1);
                delete this.users[userId];
            }
        },

        /**
         * Get items
         * @param asArray
         * @returns {*}
         */
        get: function (asArray) {
            if (asArray) {
                var result = [];
                angular.forEach(this.users, function(val) {
                    result.push(val);
                });
                return result;
            }
            return this.users;
        }
    };
});

/**
 * Messages service
 */
chatApp.factory('Message', function() {
    return {
        messageIds: [],
        messages: [],
        unseen: 0,

        /**
         * Set items
         * @param messages
         */
        set: function (messages) {
            var that = this;
            angular.forEach(messages, function (val) {
                if (that.messageIds.indexOf(val) === -1) {
                    that.messageIds.push(val.message_id);
                    that.messages[val.message_id] = val;
                }
            });
        },

        /**
         * Get items
         * @param asArray
         * @returns {*}
         */
        get: function (asArray) {
            if (asArray) {
                var result = [];
                angular.forEach(this.messages, function(val) {
                    result.push(val);
                });
                return result;
            }
            return this.messages;
        }
    };
});

/**
 * Files service
 */
chatApp.factory('File', function() {
    return {
        fileIds: [],
        files: [],
        unseen: 1,

        set: function (files) {
            var that = this;
            angular.forEach(files, function (val) {
                if (that.fileIds.indexOf(val) === -1) {
                    that.fileIds.push(val.file_id);
                    that.files[val.file_id] = val;
                }
            });
        }
    };
});

/**
 * Socket io
 *
 */
chatApp.factory('Socket', function ($rootScope) {
    var socket = io.connect();
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});