/**
 * Main menu controller
 * @param $scope
 * @param $route
 * @constructor
 */
function MenuCtrl($scope, $route) {

    $scope.$route = $route;

}

/**
 * User controller (nicklist)
 * @constructor
 */
function UserCtrl($scope, User, Socket) {

    $scope.users = User.get(true);

    Socket.on('user:connect', function (data) {
        User.set(data);
        $scope.users = User.get(true);
    });

    Socket.on('user:disconnect', function (data) {
        User.remove(data.user_id);
        $scope.users = User.get(true);
    })
}

/**
 * Compose message
 * @param $scope
 * @param Socket
 * @constructor
 */
function ComposeCtrl($scope, Socket) {

    $scope.send = function () {
        var el = $('#compose').find('textarea'),
            msg = el.val();
        if (!msg) {
            return;
        }
        Socket.emit('message', { data: msg });

        // typing is done on keydown so we need to wait for keyup
        setTimeout(function () {
            el.val('');
            type(false);
        }, 0);
    };

    var typingTimeout = null;
    function type(isTyping) {
        if (isTyping) {
            if (!typingTimeout) {
                Socket.emit('user:typing', 1);
            }
            clearTimeout(typingTimeout);
            typingTimeout = setTimeout(function () {
                type(false);
            }, 1500);
        }
        else {
            if (typingTimeout) {
                clearTimeout(typingTimeout);
                typingTimeout = null;
                Socket.emit('user:typing', 0);
            }
        }
    }

    $scope.typing = function (e) {
        var val = $.trim(e.target.value);

        if (val && !e.shiftKey && !e.ctrlKey && !e.altKey) {
            type(true);
        }
        else {
            type(false);
        }

        // prevent sending if shift, alt or ctrl held while pressing enter (obviously user is trying to do newline)
        if (e.keyCode == 13) {
            if (!e.shiftKey && !e.ctrlKey && !e.altKey) {
                e.preventDefault();
                if (val != '') {
                    $scope.send();
                }
            }
            else if (val == '') {
                e.preventDefault();
            }
        }
    }

}

/**
 *
 * @param Socket
 * @constructor
 * @param Message
 * @param $scope
 */
function ChatCtrl($scope, Socket, Message) {

    $scope.messages = Message.get(true);

    Socket.on('message', function (data) {
        Message.set(data);
        $scope.messages = Message.get(true);
    });

    Socket.on('user:typing', function (data) {
        var chat = $('#chat ul');

        console.log(data);
        if (data.isTyping) {
            chat.append('<li class="typing" id="typing-' + data.user.user_id + '"><span class="color-' + data.user.colour + '">' + data.user.username + '</span> is typing...</li>');
        }
        else {
            chat.find('#typing-' + data.user.user_id).remove();
        }
    });
}

/**
 *
 * @constructor
 */
function FilesCtrl() {

}